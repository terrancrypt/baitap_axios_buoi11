function validateInput(user) {
  isValid = true;

  isValid =
    kiemTraUsername(
      user.taiKhoan,
      arrUser,
      "spanTK",
      "Tài khoản đã tồn tại!"
    ) && kiemTraChuoi(user.taiKhoan, "spanTK", "Không được để trống!");

  isValid =
    isValid & kiemTraChuoi(user.hoTen, "spanHT", "Họ tên không hợp lệ!");

  isValid =
    isValid &
    kiemTraPassword(
      user.matKhau,
      "spanMK",
      "Mật khẩu có ít nhất 1 ký tự hoa, 1 ký tự đặc biệt, 1 ký tự số, độ dài 6-8 từ"
    );

  isValid =
    isValid & kiemTraEmail(user.email, "spanEmail", "Email không hợp lệ!");

  isValid = isValid & kiemTraUrl(user.hinhAnh, "spanHinh", "Url không hợp lệ!");

  isValid =
    isValid &
    kiemTraChuoi(user.loaiNguoiDung, "spanLoaiND", "Chọn một loại người dùng!");

  isValid =
    isValid &
    kiemTraChuoi(user.loaiNguoiDung, "spanLoaiNG", "Chọn một loại ngôn ngữ!");

  isValid =
    isValid &
    kiemTraDoDai(
      user.moTa,
      "spanMota",
      "Không được để trống, dài từ 5 đến 80 từ"
    );

  return isValid;
}

function validateUpdate(user) {
  isValid = true;

  isValid = kiemTraChuoi(user.hoTen, "spanHT", "Họ tên không hợp lệ!");

  isValid =
    isValid &
    kiemTraPassword(
      user.matKhau,
      "spanMK",
      "Mật khẩu có ít nhất 1 ký tự hoa, 1 ký tự đặc biệt, 1 ký tự số, độ dài 6-8 từ"
    );

  isValid =
    isValid & kiemTraEmail(user.email, "spanEmail", "Email không hợp lệ!");

  isValid = isValid & kiemTraUrl(user.hinhAnh, "spanHinh", "Url không hợp lệ!");

  isValid =
    isValid &
    kiemTraChuoi(user.loaiNguoiDung, "spanLoaiND", "Chọn một loại người dùng!");

  isValid =
    isValid &
    kiemTraChuoi(user.loaiNguoiDung, "spanLoaiNG", "Chọn một loại ngôn ngữ!");

  isValid =
    isValid &
    kiemTraDoDai(
      user.moTa,
      "spanMota",
      "Không được để trống, dài từ 5 đến 80 từ"
    );

  return isValid;
}

function kiemTraUsername(userName, arrUser, idSpan, alert) {
  var index = arrUser.findIndex(function (item) {
    return userName == item.taiKhoan;
  });
  if (index == -1) {
    document.getElementById(idSpan).innerText = "";
    return true;
  } else {
    document.getElementById(idSpan).innerText = alert;
    return false;
  }
}

function kiemTraChuoi(value, idSpan, alert) {
  var reg = /^[a-zA-Z ]+$/;
  var isText = reg.test(value);
  if (isText) {
    document.getElementById(idSpan).innerText = "";
    return true;
  } else {
    document.getElementById(idSpan).innerText = alert;
  }
}

function kiemTraPassword(value, idSpan, alert) {
  var expression = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{6,8}$/;
  var regex = new RegExp(expression);
  var isPassword = value;

  if (isPassword.match(regex)) {
    document.getElementById(idSpan).innerText = "";
    return true;
  } else {
    document.getElementById(idSpan).innerText = alert;
  }
}

function kiemTraEmail(value, idSpan, alert) {
  var expression =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  var regex = new RegExp(expression);
  var isEmail = value;

  if (isEmail.match(regex)) {
    document.getElementById(idSpan).innerText = "";
    return true;
  } else {
    document.getElementById(idSpan).innerText = alert;
  }
}

function kiemTraUrl(value, idSpan, alert) {
  var expression =
    /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi;
  var regex = new RegExp(expression);
  var t = value;

  if (t.match(regex)) {
    document.getElementById(idSpan).innerText = "";
    return true;
  } else {
    document.getElementById(idSpan).innerText = alert;
    return false;
  }
}

function kiemTraDoDai(value, idSpan, alert) {
  var reg = /^(?:\s*\S+(?!\S)){10,80}\s*$/;
  var isText = reg.test(value);
  if (isText) {
    document.getElementById(idSpan).innerText = "";
    return true;
  } else {
    document.getElementById(idSpan).innerText = alert;
  }
}
