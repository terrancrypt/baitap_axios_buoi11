const BASE_URL = "https://63f47c6855677ef68bbcfaf8.mockapi.io/nguoiDung";

let arrUser = [];

let fetchAPI = () => {
  axios({
    url: `${BASE_URL}`,
    method: "GET",
  })
    .then((res) => {
      (arrUser = res.data), renderNguoiDung(res.data);
      renderGiaoVien(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};

fetchAPI();

let renderNguoiDung = (userData) => {
  let contentHTML = "";
  userData.sort().map((user) => {
    contentHTML += `  
    <tr>
    <td>${user.id}</td>
        <td>${user.taiKhoan}</td>
        <td>${user.matKhau}</td>
        <td>${user.hoTen}</td>
        <td>${user.email}</td>
        <td>${user.ngonNgu}</td>
        <td>${user.loaiND}</td>
        <td><button onclick="suaNguoiDung(${user.id})" class="btn btn-success"><i class="fa fa-edit"></i></button>
        <button onclick="xoaNguoiDung(${user.id})" class="btn btn-danger"><i class="fa fa-trash"></i></button>
        </td>
    </tr>
        `;
  });

  document.querySelector("#tblDanhSachNguoiDung").innerHTML = contentHTML;
};

let renderGiaoVien = (userData) => {
  let contentHTML = "";
  let arrGV = userData.filter((user) => {
    return user.loaiND == "GV";
  });

  arrGV.sort().map((user) => {
    contentHTML += `  
        <tr>
        <td>${user.id}</td>
            <td>${user.hoTen}</td>
            <td>${user.email}</td>
            <td>${user.ngonNgu}</td>
        </tr>
            `;
  });
  document.querySelector("#tblDanhSachGiaoVien").innerHTML = contentHTML;
};

let themNguoiDung = () => {
  let user = getFormValue();
  let validate = validateInput(user);

  if (validate) {
    $("#myModal").modal("hide");
    axios({
      url: `${BASE_URL}`,
      method: "POST",
      data: user,
    })
      .then((res) => {
        console.log(res);
        fetchAPI();
      })
      .catch((err) => {
        console.log(err);
      });
  }
};

let xoaNguoiDung = (id) => {
  axios({
    url: `${BASE_URL}/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      fetchAPI();
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};

let suaNguoiDung = (id) => {
  $("#myModal").modal("show");
  document.getElementById("TaiKhoan").disabled = true;
  document.getElementById("spanTK").innerText = "Không thể thay đổi tài khoản";
  document.getElementById("spanTK").classList = "text-info";
  axios({
    url: `${BASE_URL}/${id}`,
    method: "GET",
  })
    .then((res) => {
      console.log(res);
      putFormValue(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};

let capNhatNguoiDung = () => {
  data = getFormValue();
  let validate = validateUpdate(data);

  if (validate) {
    axios({
      url: `${BASE_URL}/${data.id}`,
      method: "PUT",
      data: data,
    })
      .then((res) => {
        console.log(res);
        $("#myModal").modal("hide");
        fetchAPI();
      })
      .catch((err) => {
        console.log(err);
      });
  }
};
