let getFormValue = () =>{
    let taiKhoan = document.querySelector("#TaiKhoan").value;
    let matKhau = document.querySelector("#MatKhau").value;
    let HoTen = document.querySelector("#HoTen").value;
    let Email = document.querySelector("#Email").value;
    let loaiND = document.querySelector("#loaiNguoiDung").value;
    let ngonNgu = document.querySelector("#loaiNgonNgu").value;
    let HinhAnh = document.querySelector("#HinhAnh").value;
    let MoTa = document.querySelector("#MoTa").value;
    let id =  document.querySelector("#iID").value;

    let user = {
        id : id,
        taiKhoan: taiKhoan,
        matKhau: matKhau,
        hoTen : HoTen,
        email: Email,
        loaiND: loaiND,
        hinhAnh: HinhAnh,
        ngonNgu: ngonNgu,
        moTa: MoTa,
    }

    return user;
}

let putFormValue = (user) =>{
    document.querySelector("#iID").value = user.id;
    document.querySelector("#TaiKhoan").value = user.taiKhoan;
    document.querySelector("#MatKhau").value = user.matKhau;
    document.querySelector("#HoTen").value = user.hoTen;
    document.querySelector("#Email").value = user.email;
    document.querySelector("#loaiNguoiDung").value = user.loaiND;
    document.querySelector("#loaiNgonNgu").value = user.ngonNgu;
    document.querySelector("#HinhAnh").value = user.hinhAnh;
    document.querySelector("#MoTa").value = user.moTa;
}

